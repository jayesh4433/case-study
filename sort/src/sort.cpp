#include <iostream>
#include<vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;

#define path1 "../set3/02_students_after_round1.csv"
#define path2 "../set3/04_students_1st_install_paid.csv"
#define path3 "../set3/05_students_after_round2.csv"
class student
{
private:
	int form_no;
	string name;
	int rank_a;
	int rank_b;
	int rank_c;
	string degree;
	float degree_marks;
	int allocated_preference;
	string course_name;
	string center_id;
	int payment;
	bool reported;
	string prn;

public:
	student()
	{
		form_no=0;
		name="";
		rank_a=-1;
		rank_b=-1;
		rank_c=-1;
		degree="";
		degree_marks=0.0;
		allocated_preference=0;
		course_name="";
		center_id="";
		payment=0;
		reported=false;
		prn="";
	}
	student(int pformno,const string &pname,int pr1,int pr2,int pr3,const string &pdegree,float pmarks,
			int ppref,const string &pcname,const string& pcid,int pAmount,bool preport,const string &pprn)
		{
			form_no=pformno;
			name=pname;
			rank_a=pr1;
			rank_b=pr2;
			rank_c=pr3;
			degree=pdegree;
			degree_marks=pmarks;
			allocated_preference=ppref;
			course_name=pcname;
			center_id=pcid;
			payment=pAmount;
			reported=preport;
			prn=pprn;
		}
	void set_formno(int pformno){ form_no=pformno;}
	void set_name(string &pname){ name=pname;}
	void set_ranka(int pr1){ rank_a=pr1;}
	void set_rankb(int pr2){ rank_b=pr2;}
	void set_rankc(int pr3){ rank_c=pr3;}
	void set_degree(string &pdegree){ degree=pdegree;}
	void set_degree_marks(float pmarks){ degree_marks=pmarks;}
	void set_allocated_preference(int ppref){ allocated_preference=ppref;}
	void set_course_name(string &pcname){ course_name=pcname;}
	void set_center_id(string& pcid){ center_id=pcid;}
	void set_payment(int pAmount){ payment=pAmount;}
	void set_reported(bool preport){ reported=preport;}
	void set_prn(string &pprn){ prn=pprn;}

	int get_formno(){return form_no;}
	string& get_name(){return name;}
	int get_ranka(){return rank_a;}
	int get_rankb(){return rank_b;}
	int get_rankc(){return rank_c;}
	string& get_degree(){return degree;}
	float get_degree_marks(){return degree_marks;}
	int get_allocated_preference(){return allocated_preference;}
	string& get_course_name(){return course_name;}
	string& get_center_id(){return center_id;}
	int get_payment(){return payment;}
	bool get_reported(){ return reported;}
	string& get_prn(){ return prn;}
	void display()
	{
		cout<<form_no<<" , "<<name<<" , "<<rank_a<<" , "<<rank_b<<" , "<<rank_c<<" , "<<degree<<" , "<<degree_marks<<" , "<<allocated_preference
				<<" , "<<course_name<<" , "<<center_id<<" , "<<payment<<" , "<<reported<<" , "<<prn<<endl;
	}

};

void load_students(vector<student>& students)
{
	ifstream fin(path1);
	string line;
	if(!fin)
	{
		cout<<"failed to open student data"<<endl;
		return;
	}
	while(getline(fin,line))
	{
		stringstream str(line);
		string tok[13];
		for(int i=0;i<13;i++)
			getline(str,tok[i],',');
		student s(stoi(tok[0]) , tok[1] , stoi(tok[2]) , stoi(tok[3]) , stoi(tok[4]) , tok[5], stof(tok[6]) ,
				stoi(tok[7]) , tok[8] , tok[9] , stoi(tok[10]) , stoi(tok[11]) , tok[12]);
		//load_preference(s);
		s.display();
		students.push_back(s);
	}
	fin.close();
}
bool byFormNo(student& s1,student& s2)
{
	return (s1.get_formno()-s2.get_formno()) < 0? true:false;

}
void save_students(vector<student>& students)
{
	ofstream fout(path1);
	if(!fout)
	{
		cout<<"failed to open student data"<<endl;
		return;
	}
	for(unsigned int i=0;i<students.size();i++)
	{
		fout<<students[i].get_formno()<< "," <<students[i].get_name()<< "," <<students[i].get_ranka()<< "," <<students[i].get_rankb()<< "," <<
				students[i].get_rankc()<< "," <<students[i].get_degree()<< "," <<students[i].get_degree_marks()<< "," <<students[i].get_allocated_preference()<< "," <<
				students[i].get_course_name()<< "," <<students[i].get_center_id()<< "," <<students[i].get_payment()
				<< "," <<students[i].get_reported()<< "," <<students[i].get_prn()<<endl;
	}
	fout.close();
}
int main()
{
	vector<student> students;
	load_students(students);
	cout<<students.size();
	for(unsigned i=0;i<students.size();i++)
		students[i].display();
	cout<<endl<<endl;
	sort(students.begin(),students.end(),byFormNo);
	for(unsigned i=0;i<students.size();i++)
			students[i].display();
		cout<<endl<<endl;
	save_students(students);
	return 0;
}
