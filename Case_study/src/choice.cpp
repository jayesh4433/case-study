#include <iostream>
using namespace std;

int choice()
{
	int mchoice;
	cout<<endl<<"1. Student"<<endl;
	cout<<"2. Admin"<<endl;
	cout<<"3. Center Coordinator"<<endl;
	cout<<"0. Exit"<<endl;
	cin>>mchoice;
	return mchoice;
}
int student_Choice()
{
	int mchoice;
	cout<<endl<<"1. List courses"<<endl;
	cout<<"2. List centers"<<endl;
	cout<<"3. Give preferences"<<endl;
	cout<<"4. See allocated center/course"<<endl;
	cout<<"5. Update payment details"<<endl;
	cout<<"0. Exit"<<endl;
	cin>>mchoice;
	return mchoice;
}
int admin_Choice()
{
	int mchoice;
	cout<<endl<<"1.  List courses & eligibilities"<<endl;
	cout<<"2.  List centers & capacities"<<endl;
	cout<<"3.  List students"<<endl;
	cout<<"4.  Update student ranks"<<endl;
	cout<<"5.  Allocate centers (Round 1)"<<endl;
	cout<<"6.  Allocate centers (Round 2)"<<endl;
	cout<<"7.  List allocated students"<<endl;
	cout<<"8.  List paid students"<<endl;
	cout<<"9. List reported (at center) students"<<endl;
	cout<<"10. Generate PRN"<<endl;
	cout<<"11. List admitted students (with PRN) for given center"<<endl;
	cout<<"0.  Exit"<<endl;
	cin>>mchoice;
	return mchoice;
}
int cc_Choice()
{
	int mchoice;
	cout<<endl<<"1. List courses"<<endl;
	cout<<"2. List students"<<endl;
	cout<<"3. Update reported status"<<endl;
	cout<<"4. List admitted students (with PRN)"<<endl;
	cout<<"0. Exit"<<endl;
	cin>>mchoice;
	return mchoice;
}
