#include <iostream>
#include<vector>
#include <string>
#include <fstream>
#include <sstream>
#include<map>
#include <algorithm>

#include"case_study.h"

#define preference_csv "../preferences.csv"
#define student_csv "../students.csv"
#define eligibilities_csv "../eligibilities.csv"
#define courses_csv "../courses.csv"
#define centers_csv "../centers.csv"
#define capacity_csv "../capacities.csv"

using namespace std;

typedef enum{EXIT,STUDENT,ADMIN,CENTER_COORDINATOR}mainMenu;

typedef enum{S_LIST_COURSES=1,S_LIST_CENTERS,
	GIVE_PREFERENCES,ALLOCATED_CENTER,UPDATE_PAYMENT}studentMenu;

typedef enum{A_LIST_COURSES=1,A_LIST_CENTERS,LIST_STUDENTS,UPDATE_STUDENT_RANKS,
ALLOCATE_CENTER_R1,ALLOCATE_CENTER_R2,LIST_ALLOCATED_STUDENTS,LIST_PAID_STUDENTS,
LIST_REPORTED_STUDENTS,GENERATE_PRN,A_LIST_ADMITTED_STUDENTS}adminMenu;

typedef enum{CC_LIST_COURSES=1,CC_LIST_STUDENTS,UPDATE_REPORTED_STATUS,CC_LIST_ADMITTED_STUDENTS}centerCoordinatorMenu;

class preference
{
private:
	int form_no;
	int preference_no;
	string course_name;
	string center_id;
public:
	preference()
	{
		form_no=0;
		preference_no=0;
		course_name="";
		center_id="";
	}
	preference(int pformno,int pno,string& cname,string& cid)
	{
		form_no=pformno;
		preference_no=pno;
		course_name=cname;
		center_id=cid;
	}
	int get_preference_no(){return preference_no;}
	string& get_course_name(){return course_name;}
	string& get_center_id(){return center_id;}
	void display()
	{
		cout<<preference_no<<","<<course_name<<","<<center_id<<endl;
	}
};
class student
{
private:
	int form_no;
	string name;
	int rank_a;
	int rank_b;
	int rank_c;
	string degree;
	float degree_marks;
	int allocated_preference;
	string course_name;
	string center_id;
	int payment;
	bool reported;
	string prn;
public:
	vector<preference> preferences;
public:
	student()
	{
		form_no=0;
		name="";
		rank_a=-1;
		rank_b=-1;
		rank_c=-1;
		degree="";
		degree_marks=0.0;
		allocated_preference=0;
		course_name="NA";
		center_id="NA";
		payment=0;
		reported=false;
		prn="NA";
	}
	student(int pformno,const string &pname,int pr1,int pr2,int pr3,const string &pdegree,float pmarks,
			int ppref,const string &pcname,const string& pcid,int pAmount,bool preport,const string &pprn)
		{
			form_no=pformno;
			name=pname;
			rank_a=pr1;
			rank_b=pr2;
			rank_c=pr3;
			degree=pdegree;
			degree_marks=pmarks;
			allocated_preference=ppref;
			course_name=pcname;
			center_id=pcid;
			payment=pAmount;
			reported=preport;
			prn=pprn;
		}
	void set_formno(int pformno){ form_no=pformno;}
	void set_name(string &pname){ name=pname;}
	void set_ranka(int pr1){ rank_a=pr1;}
	void set_rankb(int pr2){ rank_b=pr2;}
	void set_rankc(int pr3){ rank_c=pr3;}
	void set_degree(string &pdegree){ degree=pdegree;}
	void set_degree_marks(float pmarks){ degree_marks=pmarks;}
	void set_allocated_preference(int ppref){ allocated_preference=ppref;}
	void set_course_name(string &pcname){ course_name=pcname;}
	void set_center_id(string& pcid){ center_id=pcid;}
	void set_payment(int pAmount){ payment=pAmount;}
	void set_reported(bool preport){ reported=preport;}
	void set_prn(string &pprn){ prn=pprn;}
	void set_preferences(preference& p){preferences.push_back(p);}

	int get_formno(){return form_no;}
	string& get_name(){return name;}
	int get_ranka(){return rank_a;}
	int get_rankb(){return rank_b;}
	int get_rankc(){return rank_c;}
	string& get_degree(){return degree;}
	float get_degree_marks(){return degree_marks;}
	int get_allocated_preference(){return allocated_preference;}
	string& get_course_name(){return course_name;}
	string& get_center_id(){return center_id;}
	int get_payment(){return payment;}
	bool get_reported(){ return reported;}
	string& get_prn(){ return prn;}
	void display()
	{
		cout<<form_no<<" , "<<name<<" , "<<rank_a<<" , "<<rank_b<<" , "<<rank_c<<" , "<<degree<<" , "<<degree_marks<<" , "<<allocated_preference
				<<" , "<<course_name<<" , "<<center_id<<" , "<<payment<<" , "<<reported<<" , "<<prn<<endl;
	}
	void displayPreferences()
	{
		for(unsigned i=0;i<preferences.size();i++)
		{
			preferences[i].display();
		}
	}

};
class eligibilitie
{
private:
	string degree;
	float min_percentage;
public:
	eligibilitie()
	{
		degree="";
		min_percentage=0.0;
	}
	eligibilitie(const string& pdegree,float pmin)
	{
		degree=pdegree;
		min_percentage=pmin;
	}
	string& get_degree(){return degree;}
	float get_min_percentage(){return min_percentage;}
};
class capacity
{
private:
	string center_id;
	string course_name;
	int capacitie;
	int filled_capcity;
public:
	capacity()
	{
		center_id="";
		course_name="";
		capacitie=0;
		filled_capcity=0;
	}
	capacity(string& pcenter_id,string& pcname,int pcapacity,int pfcapacity)
	{
		center_id=pcenter_id;
		course_name=pcname;
		capacitie=pcapacity;
		filled_capcity=pfcapacity;
	}
	void set_filled_capacity(int pcapacity){filled_capcity=pcapacity;}
	string& get_center_id(){return center_id;}
	string& get_course_name(){return course_name;}
	int get_capacity(){return capacitie;}
	int get_filled_capacity(){return filled_capcity;}
	void display()
	{
		cout<<center_id<<","<<course_name<<","<<capacitie<<","<<filled_capcity<<endl;
	}
};
class course
{
private:
		int id;
		string name;
		string fees;
		string section;

public:
		vector<eligibilitie> eligibilities;
		map<string,int> centers;

public:
		course()
		{
			id=0;
			name="";
			fees="";
			section="";
		}
		course(int pid,const string& pname,const string& pfees,const string& psection)
		{
			id=pid;
			name=pname;
			fees=pfees;
			section=psection;
		}
		void set_eligibilities(eligibilitie& e1){eligibilities.push_back(e1);}
		unsigned get_eligibilitie_size(){return eligibilities.size();}
		int get_id(){return id;};
		string& get_name(){return name;}
		string& get_fees(){return fees;}
		string& get_section(){return section;}
		void display()
		{
			cout<<id<<","<<name<<","<<fees<<","<<section<<endl;

		}
		void display_eligibilites()
		{
			for(unsigned i=0;i<eligibilities.size();i++)
			{
				cout<<eligibilities[i].get_degree()<<eligibilities[i].get_min_percentage()<<endl;
			}
		}
		void display_capacities(vector<capacity>& capacities)
		{
			map<string,int>::iterator itr = centers.begin();
			while(itr != centers.end())
			{
				capacities[itr->second].display();
				itr++;
			}
		}
};
class center
{
private:
	string center_id;
	string center_name;
	string address;
	string coordinator;
	string password;
public:
	map<string,int> courses;
public:
	center()
	{
		center_id="";
		center_name="";
		address="";
		coordinator="";
		password="";
	}
	center(string& pcid,string& pcname,string& padd,string& pcoordi,string& ppass)
	{
		center_id=pcid;
		center_name=pcname;
		address=padd;
		coordinator=pcoordi;
		password=ppass;
	}
	string& get_center_id(){return center_id;}
	string& get_center_name(){return center_name;}
	string& get_address(){return address;}
	string& get_coordinator(){return coordinator;}
	string& get_password(){return password;}
	void display()
	{
		cout<<center_id<<","<<center_name<<","<<address<<","<<coordinator<<endl;
	}
	void display_capacities(vector<capacity>& capacities)
	{
		map<string,int>::iterator itr = courses.begin();
		while(itr != courses.end())
		{
			capacities[itr->second].display();
			itr++;
		}
	}
};
class admissionSystem
{
public:
	vector<center> centers;
	vector<course> courses;
	vector<student> students;
	vector<capacity> capacities;
	vector<string> degree;
public:
	void load_degree()
	{
		ifstream fin("../degrees.txt");
		string line;
		if(!fin)
		{
			cout<<"failed to open degree data"<<endl;
			return;
		}
		while(getline(fin,line))
		{
			degree.push_back(line);
		}
	}
	void load_preference(student& s)
	{
		ifstream fin(preference_csv);
		string line;
		if(!fin)
		{
			cout<<"failed to open preference data"<<endl;
			return;
		}
		while(getline(fin,line))
		{
			stringstream str(line);
			string tok[4];
			for(int i=0;i<4;i++)
				getline(str,tok[i],',');
			if(s.get_formno() == stoi(tok[0]))
			{
				preference obj(stoi(tok[0]),stoi(tok[1]),tok[2],tok[3]);
				s.set_preferences(obj);
			}
		}
	}
	void load_students()
	{
		ifstream fin(student_csv);
		string line;
		if(!fin)
		{
			cout<<"failed to open student data"<<endl;
			return;
		}
		while(getline(fin,line))
		{
			stringstream str(line);
			string tok[13];
			for(int i=0;i<13;i++)
				getline(str,tok[i],',');

			student s(stoi(tok[0]) , tok[1] , stoi(tok[2]) , stoi(tok[3]) , stoi(tok[4]) , tok[5], stof(tok[6]) ,
					stoi(tok[7]) , tok[8] , tok[9] , stoi(tok[10]) , stoi(tok[11]) , tok[12]);
			load_preference(s);
			students.push_back(s);
		}
		fin.close();
	}
	void load_eligibilitie(course& courses)
	{
		ifstream fin(eligibilities_csv);
		string line;
		if(!fin)
		{
			cout<<"failed to open eligibilities data"<<endl;
			return;
		}
		while(getline(fin,line))
		{
			stringstream str(line);
			string tok[3];
			for(int i=0;i<3;i++)
				getline(str,tok[i],',');
			if(courses.get_name() == tok[0])
			{
				eligibilitie obj(tok[1],stof(tok[2]));
				courses.set_eligibilities(obj);
			}
		}
	}
	void load_course()
	{
		ifstream fin(courses_csv);
		string line;
		if(!fin)
		{
			cout<<"failed to open courses data"<<endl;
			return;
		}
		while(getline(fin,line))
		{
			stringstream str(line);
			string tok[4];
			for(int i=0;i<4;i++)
				getline(str,tok[i],',');

			course obj(stoi(tok[0]) , tok[1] , tok[2] , tok[3]);
			load_eligibilitie(obj);
			courses.push_back(obj);
		}

		fin.close();
	}
	void load_center()
	{
		ifstream fin(centers_csv);
		string line;
		if(!fin)
		{
			cout<<"failed to open center data"<<endl;
			return;
		}
		while(getline(fin,line))
		{
			stringstream str(line);
			string tok[5];
			for(int i=0;i<5;i++)
				getline(str,tok[i],',');

			center obj(tok[0] , tok[1] , tok[2] , tok[3] , tok[4]);
			centers.push_back(obj);
		}

		fin.close();
	}

	center* find_center(vector<center>& centers, string& center_id) {
		unsigned i;
		for(i=0; i<centers.size(); i++) {
			if(centers[i].get_center_id() == center_id)
				return &centers[i];
		}
		return NULL;
	}
	course* find_course(vector<course>& courses, string name) {
		unsigned i;
		for(i=0; i<courses.size(); i++) {
			if(courses[i].get_name() == name)
				return &courses[i];
		}
		return NULL;
	}

	void load_capacity()
	{
		ifstream fin(capacity_csv);
		string line;
		if(!fin)
		{
			cout<<"failed to open capacities data"<<endl;
			return;
		}
		while(getline(fin,line))
		{
			stringstream str(line);
			string tok[4];
			for(int i=0;i<4;i++)
				getline(str,tok[i],',');
			capacity c(tok[0],tok[1],stoi(tok[2]),stoi(tok[3]));
			capacities.push_back(c);
			center *c1=find_center(centers,tok[0]);
			c1->courses[c.get_course_name()]=capacities.size() - 1;
			course *c2=find_course(courses,tok[1]);
			c2->centers[c.get_center_id()]=capacities.size() - 1;
		}
	}

}admission;

/*Saving Data Into File*/
void save_preference(student& s)
{
	ofstream fout(preference_csv);
	string line;
	if(!fout)
	{
		cout<<"failed to open preference data"<<endl;
		return;
	}
	vector<preference>::iterator trav=s.preferences.begin();
	while(trav!=s.preferences.end())
	{
		fout<<s.get_formno()<< "," <<trav->get_preference_no()<< ","
				<<trav->get_course_name()<< "," <<trav->get_center_id()<<endl;
		trav++;
	}
}
void save_students(vector<student>& students)
{
	ofstream fout(student_csv);
	if(!fout)
	{
		cout<<"failed to open student data"<<endl;
		return;
	}
	for(unsigned int i=0;i<students.size();i++)
	{
		fout<<students[i].get_formno()<< "," <<students[i].get_name()<< "," <<students[i].get_ranka()<< "," <<students[i].get_rankb()<< "," <<
				students[i].get_rankc()<< "," <<students[i].get_degree()<< "," <<students[i].get_degree_marks()<< "," <<students[i].get_allocated_preference()<< "," <<
				students[i].get_course_name()<< "," <<students[i].get_center_id()<< "," <<students[i].get_payment()
				<< "," <<students[i].get_reported()<< "," <<students[i].get_prn()<<endl;
		save_preference(students[i]);
	}

	fout.close();
}
void save_capacity(vector<capacity>& capacities)
{
	ofstream fout(capacity_csv);
	if(!fout)
	{
		cout<<"failed to open student data"<<endl;
		return;
	}
	for(unsigned int i=0;i<capacities.size();i++)
	{
		fout<<capacities[i].get_center_id()<< "," <<capacities[i].get_course_name()
				<< "," <<capacities[i].get_capacity()<< "," <<capacities[i].get_filled_capacity()<<endl;
	}
	fout.close();
}

bool rankA(student& s1,student& s2)
{
	return (s1.get_ranka()-s2.get_ranka()) < 0? true:false;

}
bool rankB(student& s1,student& s2)
{
	return (s1.get_rankb()-s2.get_rankb()) < 0? true:false;

}
bool rankC(student& s1,student& s2)
{
	return (s1.get_rankc()-s2.get_rankc()) < 0? true:false;

}
bool byFormNo(student& s1,student& s2)
{
	return (s1.get_formno()-s2.get_formno()) < 0? true:false;

}
bool sortByCourse(student& s1,student& s2)
{
	return s1.get_course_name().compare(s2.get_course_name()) < 0? true:false;

}
bool sortByCenter(student& s1,student& s2)
{
	return s1.get_center_id().compare(s2.get_center_id()) < 0? true:false;

}
bool sortByName(student& s1,student& s2)
{
	return s1.get_name().compare(s2.get_name()) < 0? true:false;

}


void student_operation();
void admin_operation();
void cc_operation();
int check_id_pass(int id,string& pass);
int checkCenterIdPass(string& id,string& pass);
void registerStudent();
void checkEligibility(vector<int>& indexs,int index);
void givePreference(int index);
void showAllocatedCenter(int id);
void updateRank();
void round1Allocation();
void round2Allocation();
void showAllocatedStudent();
void showPaidStudent();
void showReportedStudent();
void generatePrn();
void showAdmittedStudent();
bool allocateCenter(student& s,course& c,string centerid);
void clearUnpaidStudent();
void clearAllocatedPreference();
void clearCapacity();

int main()
{
	admission.load_students();
	admission.load_course();
	admission.load_center();
	admission.load_capacity();
	admission.load_degree();
	int mchoice;
	while((mchoice=choice())!= EXIT)
	{
		switch(mchoice)
		{
		case STUDENT:
			student_operation();
			break;
		case ADMIN:
			admin_operation();
			break;
		case CENTER_COORDINATOR:
			cc_operation();
			break;
		}
	}
	//save_students(admission.students);
	//save_capacity(admission.capacities);
	return 0;
}

void student_operation()
{
	int mchoice,option;
	cout<<"1. Register new student"<<endl;
	cout<<"2. Sign in"<<endl;
	cin>>option;
	if(option==1)
	{
		registerStudent();
	}
	else if(option==2)
	{
		int id;
		string pass;
		cout<<"Enter your Form Number	:	";
		cin>>id;
		cin.ignore();
		cout<<"Enter Password	:	";
		getline(cin,pass);
		int index=check_id_pass(id,pass);
		if(index!=-1)
		{
			while((mchoice=student_Choice())!=EXIT)
			{
				switch(mchoice)
				{
				case S_LIST_COURSES:
					for(unsigned i=0;i<admission.courses.size();i++)
						admission.courses[i].display();
					break;
				case S_LIST_CENTERS:
					for(unsigned i=0;i<admission.centers.size();i++)
						admission.centers[i].display();
					break;
				case GIVE_PREFERENCES:
					givePreference(index);
					break;
				case ALLOCATED_CENTER:
					showAllocatedCenter(id);
					break;
				case UPDATE_PAYMENT:
					if((admission.students[index].get_course_name()!="NA")&&(admission.students[index].get_course_name()!="Discard"))
					{
						if(admission.students[index].get_payment()==0)
						{
							cout<<"Do your 1st Installment	:	11800 Rupees"<<endl;
							int payment;
							cin>>payment;
							admission.students[index].set_payment(payment);

						}
						else if(admission.students[index].get_payment()==11800)
						{
							int payment=11800;
							for(unsigned i=0;i<admission.courses.size();i++)
							{
								if(admission.courses[i].get_name()==admission.students[index].get_course_name())
								{
									payment=stoi(admission.courses[i].get_fees())-payment;
								}
							}
							cout<<"Do your 2nd Installment of	:	"<<payment<<" Rupees"<<endl;

							cin>>payment;
							admission.students[index].set_payment(payment);
						}
					}
					else
						cout<<"Course is not Allocated for you"<<endl;
					break;
				}
			}
		}
	}
	else
		cout<<"Invalid option";
}
void admin_operation()
{
	int mchoice;
	string usr_name,pass,str;
	str="admin";
	cout<<"Enter user name :		";
	cin>>usr_name;
	cout<<"Enter Password :		";
	cin>>pass;

	if((usr_name==str)&&(pass==str))
	{
		while((mchoice=admin_Choice())!=EXIT)
		{
			switch(mchoice)
			{
			case A_LIST_COURSES:
				for(unsigned i=0;i<admission.courses.size();i++)
				{
					admission.courses[i].display();
					admission.courses[i].display_eligibilites();
					cout<<endl<<endl;
				}
				cout<<endl<<endl;
				break;
			case A_LIST_CENTERS:
				for(unsigned i=0;i<admission.centers.size();i++)
				{
					admission.centers[i].display();
					admission.centers[i].display_capacities(admission.capacities);
					cout<<endl<<endl;
				}
				cout<<endl<<endl;
				break;
			case LIST_STUDENTS:
				for(unsigned i=0;i<admission.students.size();i++)
					admission.students[i].display();
				cout<<endl<<endl;
				break;
			case UPDATE_STUDENT_RANKS:
				updateRank();
				break;
			case ALLOCATE_CENTER_R1:
				round1Allocation();
				break;
			case ALLOCATE_CENTER_R2:
				round2Allocation();
				break;
			case LIST_ALLOCATED_STUDENTS:
				showAllocatedStudent();
				cout<<endl<<endl;
				break;
			case LIST_PAID_STUDENTS:
				showPaidStudent();
				cout<<endl<<endl;
				break;
			case LIST_REPORTED_STUDENTS:
				showReportedStudent();
				cout<<endl<<endl;
				break;
			case GENERATE_PRN:
				generatePrn();
				break;
			case A_LIST_ADMITTED_STUDENTS:
				showAdmittedStudent();
				cout<<endl<<endl;
				break;
			}
		}
	}
	else
		cout<<"Incorrect User Name or Passward"<<endl;

}
void cc_operation()
{
	int mchoice;
	string usr_name,pass,str;
	int formno;
	bool status=true;
	map<string,int>::iterator trav;
	cout<<"Enter user name :		";
	cin>>usr_name;
	cout<<"Enter Password :		";
	cin>>pass;
	int index=checkCenterIdPass(usr_name,pass);
	if(index!=-1)
	{
		while((mchoice=cc_Choice())!=EXIT)
		{
			switch(mchoice)
			{
			case CC_LIST_COURSES:
				cout<<"Course List"<<endl;

				trav=admission.centers[index].courses.begin();
				while(trav != admission.centers[index].courses.end())
				{
					cout<<trav->first<<endl;
					trav++;
				}

				break;
			case CC_LIST_STUDENTS:
				for(unsigned i=0;i<admission.students.size();i++)
				{
					if(admission.students[i].get_center_id()==admission.centers[index].get_center_id())
					{
						admission.students[i].display();
					}
				}
				break;
			case UPDATE_REPORTED_STATUS:
				cout<<"Enter Student Form Number for which status is Updated	:	";
				cin>>formno;
				for(unsigned i=0;i<admission.students.size();i++)
				{
					if(admission.students[i].get_formno()==formno)
					{
						admission.students[i].set_reported(status);
					}
				}

				break;
			case CC_LIST_ADMITTED_STUDENTS:
				for(unsigned i=0;i<admission.students.size();i++)
				{
					if(admission.students[i].get_center_id()==admission.centers[index].get_center_id())
					{
						if(admission.students[i].get_prn()!="NA")
							admission.students[i].display();
					}
				}
				break;
			}
		}
	}
	else
		cout<<"Incorrect User Name or Passward"<<endl;

}
int check_id_pass(int id,string& pass)
{
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if(id==admission.students[i].get_formno())
		{
			if(pass==admission.students[i].get_name())
				return i;
		}
	}
	return -1;
}
int checkCenterIdPass(string& id,string& pass)
{
	for(unsigned i=0;i<admission.centers.size();i++)
	{
		if(id==admission.centers[i].get_center_id())
		{
			if(pass==admission.centers[i].get_password())
				return i;
		}
	}
	return -1;
}
void registerStudent()
{
	student new_student;
	string name,degree;
	unsigned no;
	float percent;
	cin.ignore();
	cout<<"Enter Your Name	:	";
	getline(cin,name);
	cout<<endl;
	for(unsigned i=0;i<admission.degree.size();i++)
		cout<<i<<" : "<<admission.degree[i]<<endl;
	cout<<"Enter Your Degree Number"<<endl;
	cin>>no;
	if(no > (admission.degree.size()-1))
	{
		cout<<"Wrong Choice"<<endl;
		return;
	}
	degree=admission.degree[no];
	cout<<"Enter your Degree percentage	:	";
	cin>>percent;
	new_student.set_name(name);
	new_student.set_degree(degree);
	new_student.set_degree_marks(percent);
	int formno=admission.students[admission.students.size()-1].get_formno()+1;
	new_student.set_formno(formno);
	admission.students.push_back(new_student);
	cout<<"Your Form Number is	:	"<<formno<<endl;
	cout<<"Note your Form Number and use this as login ID and Your Name as Password"<<endl;
}
void checkEligibility(vector<int>& indexs,int index)
{
	for(unsigned i=0;i<admission.courses.size();i++)
	{
		for(unsigned j=0;j<admission.courses[i].eligibilities.size();j++)
		{
			if(admission.courses[i].eligibilities[j].get_degree()==admission.students[index].get_degree())
			{
				if(admission.students[index].get_degree_marks()>=admission.courses[i].eligibilities[j].get_min_percentage())
					indexs.push_back(i);
			}
		}
	}
}
void givePreference(int index)
{
	if(admission.students[index].preferences.size()>0)
	{
		for(unsigned i=0;i<admission.students[index].preferences.size();i++)
		{
			admission.students[index].preferences[i].display();
		}
		return;
	}
	vector<int>indexs;
	unsigned preference_count,course_no,institute_no;
	checkEligibility(indexs,index);
	if(indexs.size()==1)
	{
		cout<<"You are not Eligible for any course"<<endl;
		return;
	}
	cout<<"You are eligible for following courses"<<endl;
	for(unsigned i=1;i<=indexs.size();i++)
	{
		cout<<"Course Number  "<<i<<"	:	";
		cout<<admission.courses[indexs[i-1]].get_name()<<" , "
				<<admission.courses[indexs[i-1]].get_fees()<<" , "
				<<admission.courses[indexs[i-1]].get_section()<<endl;
	}
	cout<<"How may Preferences you want to give	:	";
	cin>>preference_count;
	for(unsigned i=1;i<=preference_count;i++)
	{
		int count=1;
		cout<<"Select course for preference No	:	"<<i<<endl;
		cin>>course_no;
		cout<<"Select Institute for course	:	"<<admission.courses[indexs[course_no-1]].get_name()<<endl;
		map<string,int>::iterator itr = admission.courses[indexs[course_no-1]].centers.begin();
		while(itr != admission.courses[indexs[course_no-1]].centers.end())
		{
			cout<<count++<<" ) ";
			for(unsigned j=0;j<admission.centers.size();j++)
			{
				if(admission.centers[j].get_center_id()==itr->first)
					admission.centers[j].display();
			}
			itr++;
		}
		cout<<"Enter Institute Number	:	";
		cin>>institute_no;
		itr = admission.courses[indexs[course_no-1]].centers.begin();
		for(unsigned j=0;j<institute_no-1;j++)
			itr++;
		string institute_name=itr->first;
		preference new_preference(admission.students[index].get_formno(),i,admission.courses[indexs[course_no-1]].get_name(),institute_name);
		admission.students[index].preferences.push_back(new_preference);

	}
}
void showAllocatedCenter(int id)
{
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if(id==admission.students[i].get_formno())
		{
			cout<<admission.students[i].get_center_id()<<endl;
		}
	}

}
void updateRank()
{
	int pformno,prank_a,prank_b,prank_c;
	cout<<"Enter Student Form Number	:	";
	cin>>pformno;
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if(admission.students[i].get_formno()==pformno)
		{
			cout<<"Enter Rank A marks	:	";
			cin>>prank_a;
			cout<<"Enter Rank B marks	:	";
			cin>>prank_b;
			cout<<"Enter Rank C marks	:	";
			cin>>prank_c;
			admission.students[i].set_ranka(prank_a);
			admission.students[i].set_rankb(prank_b);
			admission.students[i].set_rankc(prank_c);
		}
	}

}
void round1Allocation()
{
	/*for(unsigned i=0;i<admission.students.size();i++)
		admission.students[i].display();
	cout<<endl<<endl;*/
	for(unsigned i=1;i<=10;i++)
	{
		//cout<<"========================== "<<i<<" ===================================="<<endl;
		/*For Rank A*/
		sort(admission.students.begin(),admission.students.end(),rankA);
		for(unsigned j=0;j<admission.students.size();j++)
		{
			if(admission.students[j].get_center_id()=="NA")
			{
				if(admission.students[j].preferences.size()>=i)
				{
					string course=admission.students[j].preferences[i-1].get_course_name();

					for(unsigned k=0;k<admission.courses.size();k++)
					{
						if((admission.courses[k].get_section()=="A")&&(course==admission.courses[k].get_name()))
						{
							bool status=allocateCenter(admission.students[j],admission.courses[k],admission.students[j].preferences[i-1].get_center_id());
							if(status==true)
							{
								admission.students[j].set_allocated_preference(i);
								//admission.students[j].display();
							}
							/*else
								cout<<"seat can not be allocated"<<endl;
							cout<<endl<<endl;*/
						}
					}
				}
			}
		}

		/*For Rank B*/
		sort(admission.students.begin(),admission.students.end(),rankB);
		for(unsigned j=0;j<admission.students.size();j++)
		{
			if(admission.students[j].get_center_id()=="NA")
			{
				string course;
				if(admission.students[j].preferences.size()>=i)
				{
					course=admission.students[j].preferences[i-1].get_course_name();

					for(unsigned k=0;k<admission.courses.size();k++)
					{
						if((admission.courses[k].get_section()=="B")&&(course==admission.courses[k].get_name()))
						{
							bool status=allocateCenter(admission.students[j],admission.courses[k],admission.students[j].preferences[i-1].get_center_id());
							if(status==true)
							{
								admission.students[j].set_allocated_preference(i);
								//admission.students[j].display();
							}
							/*else
								cout<<"seat can not be allocated"<<endl;
							cout<<endl<<endl;*/
						}
					}
				}
			}
		}
		/*For Rank C*/
		sort(admission.students.begin(),admission.students.end(),rankC);
		for(unsigned j=0;j<admission.students.size();j++)
		{
			if(admission.students[j].get_center_id()=="NA")
			{
				string course;
				if(admission.students[j].preferences.size()>=i)
				{
					course=admission.students[j].preferences[i-1].get_course_name();

					for(unsigned k=0;k<admission.courses.size();k++)
					{
						if((admission.courses[k].get_section()=="C")&&(course==admission.courses[k].get_name()))
						{

							bool status=allocateCenter(admission.students[j],admission.courses[k],admission.students[j].preferences[i-1].get_center_id());
							if(status==true)
							{
								admission.students[j].set_allocated_preference(i);
								//admission.students[j].display();
							}
							/*else
								cout<<"seat can not be allocated"<<endl;
							cout<<endl<<endl;*/
						}
					}
				}
			}
		}

	}
	sort(admission.students.begin(),admission.students.end(),byFormNo);

}
void round2Allocation()
{
	cout<<"Removing Unpaid Student"<<endl;
	clearUnpaidStudent();

	cout<<"Clearing Preference of allocated"<<endl;
	clearAllocatedPreference();

	cout<<"Clearing Capacities"<<endl;
	clearCapacity();
	cout<<"Allocation Again"<<endl;
	round1Allocation();
	cout<<"Done With Allocation";

}
void showAllocatedStudent()
{
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if((admission.students[i].get_center_id()!="NA")&&(admission.students[i].get_center_id()!="Discard"))
			admission.students[i].display();
	}
}
void showPaidStudent()
{
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if(admission.students[i].get_payment()>0)
			admission.students[i].display();
	}
}
void showReportedStudent()
{
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if(admission.students[i].get_reported()==true)
			admission.students[i].display();
	}
}
void generatePrn()
{
	string course,center,name;
	int count=0;
	//sort by course
	sort(admission.students.begin(),admission.students.end(),sortByCourse);
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if((admission.students[i].get_center_id()!="NA")&&(admission.students[i].get_center_id()!="Discard"))
		{
			if(admission.students[i].get_reported()==true)
			{
				course=admission.students[i].get_course_name();
				admission.students[i].set_prn(course);
			}
		}
	}
	//sort by center
	sort(admission.students.begin(),admission.students.end(),sortByCenter);
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if((admission.students[i].get_center_id()!="NA")&&(admission.students[i].get_center_id()!="Discard"))
		{
			if(admission.students[i].get_reported()==true)
			{
				center=admission.students[i].get_center_id()+admission.students[i].get_prn();
				admission.students[i].set_prn(center);
			}
		}
	}
	//sort by name
	sort(admission.students.begin(),admission.students.end(),sortByName);
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if((admission.students[i].get_center_id()!="NA")&&(admission.students[i].get_center_id()!="Discard"))
		{
			if(admission.students[i].get_reported()==true)
			{
				name=admission.students[i].get_prn()+to_string(++count);
				admission.students[i].set_prn(name);
			}
		}
	}

	for(unsigned i=0;i<admission.students.size();i++)
		admission.students[i].display();
}
void showAdmittedStudent()
{
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if(admission.students[i].get_prn()!="NA")
			admission.students[i].display();
	}
}
bool allocateCenter(student& s,course& c,string centerid)
{
	map<string,int>::iterator itr = c.centers.begin();
	while(itr != c.centers.end())
	{
		if(itr->first==centerid)
		{
			int filled_capacity=admission.capacities[itr->second].get_filled_capacity();
			int capacity=admission.capacities[itr->second].get_capacity();
			if(filled_capacity<capacity)
			{
				s.set_center_id(centerid);
				s.set_course_name(c.get_name());
				filled_capacity+=1;
				admission.capacities[itr->second].set_filled_capacity(filled_capacity);
				return true;
			}
		}
		itr++;
	}
	return false;

}
void clearUnpaidStudent()
{
	for(unsigned i=0;i<admission.students.size();i++)
	{
		string set="Discard";
					/*Allocated Student*/					/*Student Who have not paid Fees*/
		if((admission.students[i].get_center_id()!="NA")&&(admission.students[i].get_payment()!=11800))
		{
			admission.students[i].display();
			admission.students[i].set_payment(-1);
			admission.students[i].set_center_id(set);
			admission.students[i].set_course_name(set);
			admission.students[i].display();
			cout<<endl<<endl;
		}
	}
}
void clearAllocatedPreference()
{
	for(unsigned i=0;i<admission.students.size();i++)
	{
		if((admission.students[i].get_center_id()!="NA")&&(admission.students[i].get_payment()==11800))
		{
			string set="NA";
			admission.students[i].display();
			admission.students[i].set_allocated_preference(0);
			admission.students[i].set_center_id(set);
			admission.students[i].set_course_name(set);
			admission.students[i].display();
			cout<<endl<<endl;
		}
	}
}
void clearCapacity()
{
	for(unsigned i=0;i<admission.capacities.size();i++)
	{
		admission.capacities[i].set_filled_capacity(0);
	}
}


